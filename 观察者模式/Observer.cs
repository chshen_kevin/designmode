﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    interface IObserver
    {
        void Update();
    }

    sealed class Observer : IObserver
    {
        private string _name;
        public Observer(string name)
        {
            _name = name;
        }
        public void Update()
        {
            Console.WriteLine("Observer name : {0}", _name);
        }
    }
}
