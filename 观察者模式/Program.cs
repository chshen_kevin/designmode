﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            var tony = new Observer("Tony");
            var truck = new Observer("truck");
            var sub = new Subject();
            sub.Attach(tony);
            sub.Attach(truck);
            sub.Notify();
        }
    }
}
